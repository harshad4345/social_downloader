from pytube import YouTube

class YouTubeDownloader():
    def downloader(url,only_audio=None,res=None):
        yt = YouTube(url)
        if only_audio:
            ytdownload = yt.streams.filter(only_audio=True).first()
        elif res:
            ytdownload = yt.streams.filter(res=f'{res}p').first()
        else:
            ytdownload = yt.streams.filter(res='1080p').first()
        
        response = ytdownload.download()
        print("response > ",response)

if __name__=="__main__":
    url = 'http://youtube.com/watch?v=2lAe1cqCOXo'
    YouTubeDownloader.downloader(url,True,None)